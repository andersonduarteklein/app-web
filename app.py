import flask

app = flask.Flask(__name__)

@app.route("/")
def ola():
    mensagem = "Programação de Redes com Python! " 
    return flask.render_template("index.html", mensagem=mensagem)

@app.route("/upper", methods=['POST'])
def maisucular():
    dados = flask.request.get_json()
    texto = dados['message'].upper()
    return { "message": texto }
    
app.run(host='0.0.0.0', port=80)
